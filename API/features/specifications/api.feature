Feature: Maintain user data through an API
As an user of the system
I want to register new users
So they can login with their user and password

@register_successful
Scenario Outline: Successful Register New User
  Given the API address
  When I do a request to register a new user with the following data:
  | id       | <ID>       |
  | user     | <User>     |
  | password | <Password> | 
  Then the API must return the response code

  Examples: 
  | ID  | User     | Password |
  | 12  | User 12  | Pass 12  | 
  | 13  | User 13  | Pass 13  | 
  | 14  | User 14  | Pass 14  | 
  | 15  | User 15  | Pass 15  | 

@register_unsuccessful
Scenario Outline: Unsuccessful Register New User
  Given the API address
  When I do a request to register a new user with the following data:
  | id       | <ID>       |
  | user     | <User>     |
  | password | <Password> | 
  Then the API must return the response code

  Examples: 
  | ID | User    | Password |
  | 1  | User 1  | Pass 1   | 
  | 2  | User 2  | Pass 2   | 
  | 3  | User 3  | Pass 3   | 
  | 4  | User 4  | Pass 4   | 

@retrieve_valid
Scenario Outline: Retrieve valid user information
  Given the API address
  When I try to search for an user in the system with the following data:
  | user | <User> |
  Then the system must validate if the user is registered or not
  And return the user information, when available

  Examples: 
  |User |
  |User 1|
  |User 2|
  |User 3|
  |User 4|
  |User 5|
  |User 6|
  |User 7|
  |User 8|
  |User 9|
  |User 10|

@retrieve_invalid
Scenario Outline: Retrieve invalid user information
  Given the API address
  When I try to search for an user in the system with the following data:
  | user | <User> |
  Then the system must validate if the user is registered or not
  And return the user information, when available

  Examples: 
  |User |
  |User 11|
  |User 12|
  |User 13|
  |User 14|
  |User 15|
  |User 16|
  |User 17|
  |User 18|
  |User 19|
  |User 20|