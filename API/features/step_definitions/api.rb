# frozen_string_literal: true

Given('the API address') do
  @uri_base = 'http://fakerestapi.azurewebsites.net/api/Users'
end

# ********** REGISTER NEW USER SCENARIO **********************

When('I do a request to register a new user with the following data:') do |table|
  @response = HTTParty.get(@uri_base)
  count = @response.count
  last_id = @response[count - 1]['ID']

  @data = table.rows_hash
  new_id = @data[:id]
  user = @data[:user]
  password = @data[:password]

  if new_id.to_i <= last_id.to_i
    log 'Another user already have this ID. Please choose another.'
  else
    @post_request = HTTParty.post(@uri_base,
                                  body:
                                  {
                                    "ID": new_id.to_i,
                                    "UserName": user.to_s,
                                    "Password": password.to_s
                                  }.to_json,
                                  headers: {
                                    "Content-Type": 'application/json'
                                  })
  end
end

Then('the API must return the response code') do
  if !@post_request.nil?
    if @post_request.code == 200
      log 'User successfully registered'
    else
      log "The user was not properly registered. System error #{@post_request.code}"
    end
  else
    log 'User was not registered as a duplicated ID was provided.'
  end
end

# ********** RETRIEVE USER DATA SCENARIO **********************

When('I try to search for an user in the system with the following data:') do |table|
  @user = table.rows_hash
  string_user = @user[:user]
  string_array = string_user.split(' ')
  string_id = string_array[1]

  @api_response = HTTParty.get(@uri_base, query: { ID: string_id })
end

Then('the system must validate if the user is registered or not') do
  if @api_response.code == 200
    @user_found = true
    log 'User found!'
  else
    @user_found = false
  end
end

And('return the user information, when available') do
  if @user_found
    user_id = @api_response['ID']
    log 'ID: ' + user_id.to_s
    log 'Usuário: ' + @api_response['UserName']
    log 'Password: ' + @api_response['Password']
  else
    if @api_response.code == 404
      log 'Error 404. User was not found. Not even C-3PO could find him. :('
    else
      log "System response: #{@api_response.code}"
    end
  end
end
