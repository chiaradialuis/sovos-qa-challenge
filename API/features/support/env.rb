# frozen_string_literal: true

require 'capybara'
require 'capybara/cucumber'
require 'selenium-webdriver'
require 'rspec'
require 'httparty'
require 'io/console'
require 'json'

Capybara.configure do |config|
  config.default_driver = :selenium_chrome
end
