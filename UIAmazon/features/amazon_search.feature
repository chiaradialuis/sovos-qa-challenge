Feature: Search
  For me to be able to buy on amazon
  I need to search for products

  @succesful_search
  Scenario Outline: Successful Search
    Given that I am on "https://www.amazon.com"
    When I search for a product with the following data:
    | product | <Product> | 
    Then I need to check if the product was found:
    | product | <Product> | 
    And verify if the search was performed as per the input

    Examples:
    | Product | 
    | "FIFA 20 (PS4)" |


  @unsuccesful_search
  Scenario Outline: Unsuccessful results
    Given that I am on "https://www.amazon.com"
    When I search for a product with the following data:
    | product | <Product> |
    Then I need to check if no results appeared

    Examples:
    | Product | 
    | ASDFG1234* |

  @correct_category
  Scenario: Search on correct category
    Given that I am on "https://www.amazon.com"
    When I search for a product with a selected category with the following data:
    | product  | <Product>  |
    | category | <Category> | 
    Then I need to check if the product was found:
    | product | <Product> | 
    And verify if the search was performed as per the input

    Examples:
    |    Product      |  Category   |        
    | "FIFA 20 (PS4)" | Video Games | 

  @wrong_category
  Scenario: Search on wrong category
    Given that I am on "https://www.amazon.com"
    When I search for a product with a selected category with the following data:
    | product  | <Product>  |
    | category | <Category> | 
    Then I need to check if no results appeared

    Examples:
    |    Product      |   Category   |        
    | "FIFA 20 (PS4)" | Pet Supplies | 

  @add_to_cart
  Scenario: Search Product and Add to Cart
    Given that I found a product on Amazon with the following data:
    | product | <Product> | 
    | website | <Website> |
    When I click to add to the cart
    Then I must see a confirmation message
    And the subtotal must match the sum of all products in the cart

   Examples:
    |     Product     |        Website         |
    | "FIFA 20 (PS4)" | https://www.amazon.com |