# frozen_string_literal: true

Given('that I am on {string}') do |website|
  visit website
end
# *************** Scenario: Successful Search ****************

When('I search for a product with the following data:') do |table|
  @data = table.rows_hash
  product = @data[:product]
  within('#nav-search') do
    find('input[id=twotabsearchtextbox]').set product
    find('input[type=submit]').click
  end
  @product = product
end

Then('I need to check if the product was found:') do |table|
  @data = table.rows_hash
  result = @data[:product]
  within('.s-search-results') do
    result = result[1, (result.length - 2)]
    product_found = find('a', text: result, match: :first).text
    # product_found = find('.s-line-clamp-2').text
    @product_found = product_found
    expect(product_found).to have_content result
  end
end

Then('verify if the search was performed as per the input') do
  search_string = find('a', text: @product_found, match: :first).text
  expect(search_string).to have_content @product_found
end

# ********************  Scenario: Unsuccessful results **************

Then('I need to check if no results appeared') do
  within('span[data-cel-widget=MAIN-TOP_BANNER_MESSAGE]') do
    search_string = find('.a-spacing-top-medium').text
    expect(search_string).to have_content 'No results for ' + @product
  end
end

# ***************   Scenario: Search on correct and wrong category **************

When('I search for a product with a selected category with the following data:') do |table|
  @data = table.rows_hash
  product = @data[:product]
  category = @data[:category]
  within('#nav-search') do
    find('input[id=twotabsearchtextbox]').set product
    within('#nav-search-dropdown-card') do
      category_list = find('#searchDropdownBox', visible: false)
      category_list.find('option', text: category).select_option
    end
    find('input[type=submit]').click
  end
  @product = product
  @category = category
end

# ***************   Scenario: Search Product and Add to Cart **************

Given('that I found a product on Amazon with the following data:') do |table|
  @data = table.rows_hash
  product_to_search = @data[:product]
  website = @data[:website]
  visit website
  within('#nav-search') do
    find('input[id=twotabsearchtextbox]').set product_to_search
    find('input[type=submit]').click
  end
  product_to_search = product_to_search[1, (product_to_search.length - 2)]

  within('.s-search-results') do
    product_found = find('a', text: product_to_search, match: :first).text
    @product_found = product_found
    expect(product_found).to have_content product_to_search
    find('a', text: product_to_search, match: :first).click
  end

  @price = find('#priceblock_ourprice').text
end

When('I click to add to the cart') do
  find('input[id=add-to-cart-button]').click
end

Then('I must see a confirmation message') do
  confirmation = find('#huc-v2-order-row-confirm-text').text
  expect(confirmation).to eql 'Added to Cart'
  @cart_count = find('#nav-cart-count').text
  find('#nav-cart-count').click
end

Then('the subtotal must match the sum of all products in the cart') do
  within('#activeCartViewForm') do
    within('div[id^=sc-item]') do
      @product_price = find('.a-spacing-small').text
      @product_price = @product_price[1, @product_price.length]
      @product_price = @product_price.to_d
    end
    subtotal = find('#sc-subtotal-amount-activecart').text
    subtotal = subtotal[1, subtotal.length]
    subtotal = subtotal.to_d
    expect(subtotal).to eql @product_price
  end
end
